FROM python as build

ENV PYTHONUNBUFFERED=1

RUN mkdir /aiohttp_chat

WORKDIR /aiohttp_chat

COPY . .

RUN pip install -r requirements/requirements.txt


FROM build as test

RUN pip install -r requirements/test.txt

FROM build as dev

RUN pip install -r requirements/development.txt

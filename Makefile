PROJECT_NAME=aiohttp_test_2
SERVER_PORT=8000

.EXPORT_ALL_VARIABLES:

# Runs application development on docker. Builds, creates, starts containers for a service. | Common
run: build
	@docker-compose up


# Builds docker image
build:
	@docker build --no-cache -t $(PROJECT_NAME)_web .


# Runs tests
tests:
	@docker build --no-cache -t $(PROJECT_NAME)_web --target test .
	@docker-compose up -d
	@docker exec -it aiohttp_web pytest test -v

# AIOHTTP CHAT

## System requirements

* make
* docker
* docker-compose

# Usage

### Run

To start the project in development mode, run the following command:

```
make run
```

To build docker image without running server:

```
make build
```

To run tests using pytest:

```
make tests
```

## Software

- python3.9


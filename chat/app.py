"""Creating app module."""
import os
import pathlib
from collections import defaultdict

import aiohttp_jinja2
import aiohttp_session
import jinja2
from aiohttp import web
from aiohttp_session.redis_storage import RedisStorage
from aiojobs.aiohttp import setup as setup_aiojob
from aioredis import create_redis_pool

from chat.context_processors import base_login_name
from chat.middlewares import error_middleware
from chat.mongo_init_db import init_mongo, close_mongo
from chat.routes import add_routes
from chat.screenshoter import init_screenshoter, close_screenshoter

SERVER_PORT = os.getenv('SERVER_PORT', 80)
ROOT_FOLDER = pathlib.Path(__file__).parent.absolute()


async def create_app():
    """Prepare application."""
    app = web.Application()

    app.ws_storage = defaultdict(dict)

    redis_port = os.getenv('REDIS_PORT', '6379')
    redis_host = os.getenv('REDIS_HOST', 'localhost')

    redis_pool = await create_redis_pool((redis_host, redis_port))
    redis_storage = RedisStorage(redis_pool=redis_pool, max_age=60 * 60 * 5)
    aiohttp_session.setup(app, redis_storage)

    aiohttp_jinja2.setup(app=app, loader=jinja2.FileSystemLoader(ROOT_FOLDER.joinpath('templates')),
                         context_processors=[base_login_name, aiohttp_jinja2.request_processor])
    app.middlewares.append(error_middleware)

    add_routes(app)
    setup_aiojob(app)

    app.on_startup.append(init_screenshoter)
    app.on_startup.append(init_mongo)

    app.on_shutdown.append(close_mongo)
    app.on_shutdown.append(close_screenshoter)

    if os.environ.get('DEBUG'):
        import aiohttp_debugtoolbar
        aiohttp_debugtoolbar.setup(app=app, intercept_redirects=False)

    return app


if __name__ == '__main__':
    app = create_app()
    web.run_app(app=app, port=SERVER_PORT)

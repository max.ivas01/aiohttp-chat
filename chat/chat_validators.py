"""Validators module."""
import re

from chat.errors import ValidationError


def user_validator(data):
    """User data validator."""
    if 'login' not in data:
        raise ValidationError('Missing login')
    if 'password' not in data:
        raise ValidationError('Missing password')
    if 'password_check' not in data:
        raise ValidationError('Missing password confirmation')

    if len(data['login']) < 4:
        raise ValidationError('Username too short! Must be at least 4 digits')
    if len(data['login']) > 14:
        raise ValidationError('Username too long! Must be less than 15 digits')

    if not data['password'] == data['password_check']:
        raise ValidationError("Passwords doesn't match")
    if len(data['password']) < 6:
        raise ValidationError("Password too short! Must be at least 6 digits")

    if not re.match(r'^[a-zA-Z0-9_-]+$', data['login']):
        raise ValidationError('Only letters, numbers and "_" "-" are allowed!')


def room_name_validator(data):
    """Room name validator."""
    if 'room_name_create' not in data:
        raise ValidationError('Missing room name')

    if len(data['room_name_create']) < 3:
        raise ValidationError('Room name too short! Must be at least 3 digits')
    if len(data['room_name_create']) > 24:
        raise ValidationError('Room name too long! Must be less than 25 digits')


def index_validator(create_room_name, join_room_name):
    """IndexView create/join room validator."""
    if create_room_name and join_room_name:
        raise ValidationError('Choose one: create or join')
    if not create_room_name and not join_room_name:
        raise ValidationError('Missing room name')

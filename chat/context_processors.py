"""Context processors module."""
from aiohttp_session import get_session


async def base_login_name(request):
    """Get user name for base.html."""
    session = await get_session(request)
    user = session.get("user")
    return {'user': user}

"""Custom exceptions."""


class InvalidPassword(Exception):
    """Invalid password exception."""

    def __init__(self):
        """Init method."""
        super(InvalidPassword, self).__init__('Password is incorrect. Try again')


class UserExists(Exception):
    """User already exists in DB exception(SignUp)."""

    def __init__(self):
        """Init method."""
        super(UserExists, self).__init__('User already exists. Try another nickname.')


class UserDoesNotExist(Exception):
    """User does not exist exception(LogIn)."""

    def __init__(self):
        """Init method."""
        super(UserDoesNotExist, self).__init__("User with that nickname doesn't exists.")


class ValidationError(Exception):
    """Post data validation exception."""

    def __init__(self, message='Validation error'):
        """Init method with custom error message."""
        super(ValidationError, self).__init__(message)


class RoomExists(Exception):
    """Room already exists in DB exception(Index)."""

    def __init__(self):
        """Init method."""
        super(RoomExists, self).__init__('Room with this name already exists.')


class RoomDoesntExist(Exception):
    """Room doesn't exists in DB exception(Index)."""

    def __init__(self):
        """Init method."""
        super(RoomDoesntExist, self).__init__("Room with this name doesn't exists.")

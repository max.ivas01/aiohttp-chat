"""Helpers module."""
from aiohttp import web
from aiohttp_session import get_session


def login_required(func):
    """Allow only auth users."""

    async def wrapped(self, *args, **kwargs):
        session = await get_session(self.request)
        if not session.get('user'):
            return web.HTTPFound('/login', reason='Login required.')
        return await func(self, *args, **kwargs)

    return wrapped


def anonymous_required(func):
    """Allow only not auth users."""

    async def wrapped(self, *args, **kwargs):
        session = await get_session(self.request)
        if session.get('user'):
            return web.HTTPFound('/', reason='Anonymous required.')
        return await func(self, *args, **kwargs)

    return wrapped

"""Middlewares module."""
import aiohttp_jinja2
from aiohttp import web


@web.middleware
async def error_middleware(request, handler):
    """Catch http exception and render error.html."""
    try:
        return await handler(request)
    except web.HTTPException:
        return aiohttp_jinja2.render_template('error.html', request, {}, status=500)

"""MongoDB wrapper."""
import os
from hashlib import sha256

from motor.motor_asyncio import AsyncIOMotorClient

from chat.errors import (UserDoesNotExist, InvalidPassword, RoomDoesntExist, UserExists, RoomExists)

mongodb_host = os.getenv('MONGODB_HOST', 'localhost')
mongodb_port = os.getenv('MONGODB_PORT', '27017')


class MongoWrapper:
    """Mongo wrapper."""

    def __init__(self, url):
        """DB client initialization."""
        self.client = AsyncIOMotorClient(url)
        self.chat_db = self.client[url.split('/')[-1]]
        self.users = self.chat_db['users']
        self.rooms = self.chat_db['rooms']

    async def create_user(self, post_data):
        """Create and validate user method."""
        user_data = dict(login=post_data['login'], password=self.__hash_pwd(post_data['password']))

        await self.__validate_user(user_data['login'])
        await self.users.insert_one(user_data)

    async def find_user(self, post_data):
        """Find and validate user method."""
        user_data = dict(login=post_data['login'], password=post_data['password'])
        user = await self.users.find_one({'login': user_data['login']})

        if not user:
            raise UserDoesNotExist

        if not user['password'] == self.__hash_pwd(user_data['password']):
            raise InvalidPassword

        return user

    async def create_room(self, room_name):
        """Create and validate room method."""
        await self.__validate_room(room_name)
        slug_name = self.__get_slug_name(room_name)
        await self.rooms.insert_one({'room_name': room_name,
                                     'slug_name': slug_name,
                                     'users': [],
                                     'messages': []})
        return slug_name

    async def join_room(self, room_name):
        """Raise exception if room exists or return slug of new room."""
        room = await self.rooms.find_one({'room_name': room_name})
        if not room:
            raise RoomDoesntExist
        return self.__get_slug_name(room_name)

    async def write_message(self, slug_name, login, message):
        """Save messages to DB."""
        await self.rooms.update_one({'slug_name': slug_name},
                                    {'$push': {'messages': {
                                        'nickname': login,
                                        'message': message}
                                    }})

    async def messages_history(self, slug_name):
        """Return messages history for room from DB."""
        return await self.rooms.find_one({'slug_name': slug_name})

    async def update_users(self, slug_name, users):
        """Update active users in DB."""
        await self.rooms.update_one({'slug_name': slug_name},
                                    {'$set': {'users': users}})

    async def get_rooms(self):
        """Return list of active rooms."""
        rooms = await self.rooms.find().to_list(length=100)
        return rooms

    async def __validate_user(self, user_login):
        """User validation by unique login."""
        if await self.users.find_one({'login': user_login}):
            raise UserExists

    async def __validate_room(self, room_name):
        """Room validation by unique name."""
        room_name = self.__get_slug_name(room_name)
        if await self.rooms.find_one({'slug_name': room_name}):
            raise RoomExists

    @staticmethod
    def __hash_pwd(pwd):
        """Password hasher(sha-256)."""
        return sha256(pwd.encode('ascii')).hexdigest()

    @staticmethod
    def __get_slug_name(name):
        """Return name without punctuation symbols and spaces."""
        return '_'.join(name.split()).lower().translate({ord(x): '_' for x in '.,/!?=\'\"'})


async def init_mongo(app):
    """Initialize connection."""
    app.db = MongoWrapper(f'mongodb://{mongodb_host}:{mongodb_port}/chat')


async def close_mongo(app):
    """Close connection."""
    app.db.client.close()

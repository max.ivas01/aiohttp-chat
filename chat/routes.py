"""Creating routes module."""
import os

from chat.views import (IndexView, LoginView, LogOutView, SignUpView, ChatView, WebSocket)

STATIC_PATH = os.path.join(os.path.dirname(__file__), "static")


def add_routes(app):
    """Add routes to app."""
    app.router.add_route(method='GET', path='/', handler=IndexView)
    app.router.add_route(method='POST', path='/', handler=IndexView)

    app.router.add_route(method='GET', path='/login', handler=LoginView)
    app.router.add_route(method='POST', path='/login', handler=LoginView)

    app.router.add_route(method='GET', path='/logout', handler=LogOutView)

    app.router.add_route(method='GET', path='/sign_up', handler=SignUpView)
    app.router.add_route(method='POST', path='/sign_up', handler=SignUpView)

    app.router.add_route(method='GET', path='/room/{room_name}', handler=ChatView)

    app.router.add_route(method='GET', path='/ws', handler=WebSocket)

    app.router.add_static(prefix='/static/', path=STATIC_PATH, name='static')
    app['static_root_url'] = '/static'

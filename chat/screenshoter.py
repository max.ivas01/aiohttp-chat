from pyppeteer import launch


async def init_screenshoter(app):
    app.browser = await launch(args=['--disable-gpu', '--no-sandbox', '--single-process'])


async def close_screenshoter(app):  # TODO FIX CLOSING BROWSER
    await app.browser.close()

$("#chat").animate({scrollTop: $('#chat').prop("scrollHeight")}, 0);

const ws = new WebSocket('ws://' + window.location.host + '/ws');

ws.onmessage = function (data) {
    const message = JSON.parse(data.data);

    if (message.screenshot !== undefined) {
        var image = new Image();
        image.src = 'data:image/png;base64, ' + message.screenshot;
        image.width = "100"
        image.height = "100"
        $('#id_' + message.id).append('<br>').append(image);
    } else {
        const messageHtml = $('<div id="id_' + message.id + '" class="alert alert-' + message.message_type + '" role="alert">' +
            '                        <b>' + message.nickname + '</b> ' + message.message + '</div>');
        $('#chat').append(messageHtml);
    }

    $("#chat").animate({scrollTop: $('#chat').prop("scrollHeight")}, 0);
};

$(this).keypress(function (e) {
    if (e.which == 13) {
        $('#send').click()
    }
});

$('#send').click(function () {
    const input = $('#message');
    const message = input.val();
    if (message !== '') {
        ws.send(message);
        input.val('');
    }
});

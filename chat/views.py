"""Creating views module."""
import re
from uuid import uuid4

import aiohttp_jinja2
from aiohttp import web
from aiohttp.http_websocket import WSMsgType
from aiohttp_session import get_session
from aiojobs.aiohttp import spawn
from pyppeteer.errors import PageError, NetworkError

from chat.chat_validators import user_validator, room_name_validator, index_validator
from chat.errors import (ValidationError, RoomExists, RoomDoesntExist, InvalidPassword, UserDoesNotExist, UserExists)
from chat.helpers import login_required, anonymous_required


class IndexView(web.View):
    """Index view."""

    @aiohttp_jinja2.template('index.html')
    async def get(self):
        """Get method."""
        rooms = await self.request.app.db.get_rooms()

        for room in rooms:
            users = [*self.request.app.ws_storage[room['slug_name']].values()]
            room_users = [user[1] for user in users]
            await self.request.app.db.update_users(room['slug_name'],
                                                   room_users)

        return {'active_rooms': rooms}

    @login_required
    @aiohttp_jinja2.template('index.html')
    async def post(self):
        """Post method to create room."""
        post_data = await self.request.post()
        rooms = await self.request.app.db.get_rooms()
        room_name_create = post_data['room_name_create']
        room_name_join = post_data['room_name_join']
        slug_name = ''

        try:
            index_validator(room_name_create, room_name_join)
        except ValidationError as error:
            return {'errors': error, 'active_rooms': rooms,
                    'room_name_create': room_name_create,
                    'room_name_join': room_name_join}

        if room_name_create:

            try:
                room_name_validator(post_data)
            except ValidationError as error:
                return {'errors': error, 'active_rooms': rooms,
                        'room_name_create': room_name_create}

            try:
                slug_name = await self.request.app.db.create_room(room_name_create)
            except RoomExists as error:
                return {'errors': error,
                        'room_name_create': room_name_create,
                        'active_rooms': rooms}

        elif room_name_join:
            try:
                slug_name = await self.request.app.db.join_room(room_name_join)
            except RoomDoesntExist as error:
                return {'errors': error,
                        'room_name_join': room_name_join,
                        'active_rooms': rooms}
        if not slug_name:
            return web.HTTPFound('/error')
        return web.HTTPFound(f'/room/{slug_name}')


class LoginView(web.View):
    """Login View."""

    @anonymous_required
    @aiohttp_jinja2.template('login.html')
    async def get(self):
        """Get method."""
        return {}

    @anonymous_required
    @aiohttp_jinja2.template('login.html')
    async def post(self):
        """Post method."""
        post_data = await self.request.post()

        try:
            await self.request.app.db.find_user(post_data)
        except (InvalidPassword, UserDoesNotExist) as error:
            return {'errors': error,
                    'login': post_data['login']}

        session = await get_session(self.request)
        session['user'] = str(post_data['login'])

        return web.HTTPFound('/')


class LogOutView(web.View):
    """Logout View."""

    @login_required
    async def get(self):
        """Get method."""
        (await get_session(self.request)).invalidate()

        return web.HTTPFound('/')


class SignUpView(web.View):
    """Registration View."""

    @anonymous_required
    @aiohttp_jinja2.template('sign_up.html')
    async def get(self):
        """Get method."""
        return {}

    @anonymous_required
    @aiohttp_jinja2.template('sign_up.html')
    async def post(self):
        """Post method."""
        post_data = await self.request.post()

        try:
            user_validator(post_data)
        except ValidationError as error:
            return {'errors': error,
                    'login': post_data['login']}

        try:
            await self.request.app.db.create_user(post_data)
        except UserExists as error:
            return {'errors': error,
                    'login': post_data['login']}

        session = await get_session(self.request)
        session['user'] = str(post_data['login'])

        return web.HTTPFound('/')


class ChatView(web.View):
    """ChatRoom view."""

    @login_required
    @aiohttp_jinja2.template('chat_room.html')
    async def get(self):
        """Get method."""
        session = await get_session(self.request)
        slug_name = str(self.request.url).split('/')[-1]
        session['slug_name'] = slug_name
        messages = await self.request.app.db.messages_history(slug_name)
        if not messages:
            return web.HTTPFound('/error')
        return {'messages': messages['messages']}


async def screen(url, ws_dict, browser, message_id):
    """Make screenshot and send ws message."""
    if not url.startswith('https://') and not url.startswith('http://'):
        url = 'https://' + url
    try:
        page = await browser.newPage()
        await page.goto(url)
        screenshot = await page.screenshot(encoding='base64')
        await page.close()
    except (PageError, NetworkError):
        pass
    else:
        for ws, _ in ws_dict.values():
            if not ws.closed:
                await ws.send_json({
                    'screenshot': screenshot,
                    'id': message_id
                })


class WebSocket(web.View):
    """WebSocket service view."""

    @login_required
    async def get(self):
        """Return ws method."""
        ws = web.WebSocketResponse()
        await ws.prepare(self.request)

        session = await get_session(self.request)
        login = session['user']
        slug_name = session['slug_name']

        self.request.app.ws_storage[slug_name][id(ws)] = (ws, login)

        while not ws.closed:
            message_id = str(uuid4())
            message = await ws.receive()
            if message.type == WSMsgType.TEXT:

                await self.request.app.db.write_message(slug_name, login, message.data)

                url_pattern = r'[(http:\/\/)|\w]*?[\w]*?\.?[-\/\w]*\.\w*[(\/{1})]?[#-\.\/\w]*[(\/{1,})]?'
                url = re.search(url_pattern, message.data)

                if url:
                    await spawn(self.request, screen(url.group(), self.request.app.ws_storage[slug_name],
                                                     self.request.app.browser, message_id))
                for client, _ in self.request.app.ws_storage[slug_name].values():

                    if not client.closed:
                        await client.send_json({
                            'nickname': login + ': ',
                            'message': message.data,
                            'message_type': 'success',
                            'id': message_id
                        })

        del self.request.app.ws_storage[slug_name][id(ws)]

        message_id = str(uuid4())

        for client, _ in self.request.app.ws_storage[slug_name].values():
            if not client.closed:
                await client.send_json({
                    'nickname': '',
                    'message': f'{login} has left the chat!',
                    'message_type': 'danger',
                    'id': message_id
                })

        return ws

"""Pytest fixtures."""
import os
from unittest.mock import patch

import pytest

from chat.app import create_app
from chat.mongo_init_db import MongoWrapper

mongodb_host = os.getenv('MONGODB_HOST', 'localhost')
mongodb_port = os.getenv('MONGODB_PORT', '27017')


async def init_test_mongo(app):
    app.db = MongoWrapper(f'mongodb://{mongodb_host}:{mongodb_port}/chat_test')
    assert app.db.chat_db.name == 'chat_test'
    app.db.users.delete_many({})
    app.db.rooms.delete_many({})
    return app.db


@pytest.fixture
async def client(aiohttp_client):
    with patch('chat.app.init_mongo', new=init_test_mongo):
        return await aiohttp_client(await create_app())


@pytest.fixture
async def logged_client(aiohttp_client):
    with patch('chat.app.init_mongo', new=init_test_mongo):
        client = await aiohttp_client(await create_app())
        await client.post('/sign_up', data={'login': 'Test_User', 'password': '123123', 'password_check': '123123'})
        return client


@pytest.fixture
async def get_test_db(aiohttp_client):
    with patch('chat.app.init_mongo', new=init_test_mongo):
        app = await create_app()
        app.db = await init_test_mongo(app)
        await app.db.users.delete_many({})
        await app.db.rooms.delete_many({})
        db = app.db
        yield db
        await db.client.drop_database('chat_test')


@pytest.fixture
async def get_filled_db(get_test_db):
    db = get_test_db
    await db.users.delete_many({})
    await db.rooms.delete_many({})

    rooms_names = ['Test_room', 'Test_room.2', 'Test room 3', 'Test!room?4']
    for name in rooms_names:
        room = await db.create_room(name)
        await db.write_message(room, 'Test User', 'Test Message')

    users_names = ['Test_User', 'Test User']
    for name in users_names:
        await db.create_user({'login': name, 'password': '123123'})

    yield db

"""Tests for chat."""
import pytest
from test.utils import path
from chat.errors import RoomExists, UserExists


class TestApi:

    @pytest.mark.parametrize('logged, method, redirect, post_data, response_path, reason, error', [
        (False, 'get', False, None, '/', '', ''),
        (True, 'get', False, None, '/', 'Anonymous required.', ''),
        (False, 'post', True, {'room_name_join': '', 'room_name_create': 'Test_Room'}, '/login', 'Login required.', ''),
        (False, 'post', True, {'room_name_join': 'Test_Room', 'room_name_create': 'Test_Room'}, '/login',
         'Login required.', ''),
        (False, 'post', True, {'room_name_join': '', 'room_name_create': ''}, '/login', 'Login required.', ''),
        (True, 'post', True, {'room_name_join': '', 'room_name_create': 'Test_Room'}, '/test_room', 'Found', ''),
        (True, 'post', False, {'room_name_join': 'Test_Room', 'room_name_create': 'Test_Room'}, '/', '',
         'Choose one: create or join.'),
        (True, 'post', False, {'room_name_join': '', 'room_name_create': ''}, '/', '', 'Missing room name.')
    ])
    async def test_index(self, client, logged_client, logged, method, redirect,
                         post_data, response_path, reason, error):

        cl = client
        if logged:
            cl = logged_client

        response = await cl.__getattribute__(method)('/', data=post_data)
        assert response.status == 200

        if redirect:
            assert len(response.history) == 1
            assert response.history[0].status == 302
            assert response.history[0].reason == reason

        else:
            assert len(response.history) == 0

        assert path(response.url) == response_path

    @pytest.mark.parametrize('logged, method, redirect, post_data, response_path, reason', [
        (False, 'get', False, None, '/login', ''),
        (True, 'get', True, None, '/', 'Anonymous required.'),
        (False, 'post', True, {'login': 'Test_User', 'password': '123123'}, '/', 'Found'),
        (True, 'post', True, {'login': 'Test_User', 'password': '123123'}, '/', 'Anonymous required.')
    ])
    async def test_login(self, client, logged_client, logged, method, redirect, post_data, response_path, reason):

        cl = client
        if logged:
            cl = logged_client

        response = await cl.__getattribute__(method)('/login', data=post_data)
        assert response.status == 200

        if redirect:
            assert len(response.history) == 1
            assert response.history[0].status == 302
            assert response.history[0].reason == reason

            if not logged:
                assert 'AIOHTTP_SESSION' in response.history[0].cookies
        else:
            assert len(response.history) == 0

        assert path(response.url) == response_path

    @pytest.mark.parametrize('logged, method, redirect, post_data, response_path, reason, error', [
        (False, 'get', False, None, '/sign_up', '', ''),
        (True, 'get', True, None, '/', 'Anonymous required.', ''),
        (False, 'post', True, {'login': 'Test_User_2', 'password': '123123', 'password_check': '123123'}, '/',
         'Found', ''),
        (False, 'post', False, {'login': 'Test_User', 'password': '123123', 'password_check': '123123'}, '/sign_up',
         '', 'User already exists.'),
        (True, 'post', True, {'login': 'Test_User', 'password': '123123', 'password_check': '123123'}, '/',
         'Anonymous required.', '')
    ])
    async def test_signup(self, client, logged_client, logged, method, redirect,
                          post_data, response_path, reason, error):

        cl = client
        if logged:
            cl = logged_client

        response = await cl.__getattribute__(method)('/sign_up', data=post_data)
        assert response.status == 200

        if error:
            assert error in await response.text()

        if redirect:
            assert len(response.history) == 1
            assert response.history[0].status == 302
            assert response.history[0].reason == reason

            if not logged:
                assert 'AIOHTTP_SESSION' in response.history[0].cookies
        else:
            assert len(response.history) == 0

        assert path(response.url) == response_path

    @pytest.mark.parametrize('logged, method, redirect, status, response_path, reason, error', [
        (False, 'get', True, 200, '/login', 'Login required.', ''),
        (True, 'get', False, 500, '/ws', '', 'Oops...Something went wrong.'),
        (False, 'post', False, 500, '/ws', '', 'Oops...Something went wrong.'),
        (True, 'post', False, 500, '/ws', '', 'Oops...Something went wrong.')
    ])
    async def test_ws(self, client, logged_client, logged, method, redirect, status, response_path, reason, error):

        cl = client
        if logged:
            cl = logged_client

        response = await cl.__getattribute__(method)('/ws', data={'some_key': 'some_value'})
        assert response.status == status

        if error:
            assert error in await response.text()

        if redirect:
            assert len(response.history) == 1
            assert response.history[0].status == 302
            assert response.history[0].reason == reason
        else:
            assert len(response.history) == 0

        assert path(response.url) == response_path

    @pytest.mark.parametrize('logged, method, redirect, status, response_path, reason, error', [
        (False, 'get', True, 200, '/login', 'Login required.', ''),
        (True, 'get', False, 200, '', '', ''),
        (False, 'post', False, 500, '', '', 'Oops...Something went wrong.'),
        (True, 'post', False, 500, '', '', 'Oops...Something went wrong.')
    ])
    async def test_rooms(self, client, logged_client, logged, method, redirect,
                         status, response_path, reason, error, get_filled_db):

        cl = client
        if logged:
            cl = logged_client

        db = get_filled_db
        rooms = await db.get_rooms()

        for room in rooms:

            response = await cl.__getattribute__(method)('/room/' + room['slug_name'], data={'some_key': 'some_value'})
            assert response.status == status

            if error:
                assert error in await response.text()

            if redirect:
                assert len(response.history) == 1
                assert response.history[0].status == 302
                assert response.history[0].reason == reason
                assert path(response.url) == response_path
            else:
                assert len(response.history) == 0
                assert path(response.url) == '/' + room['slug_name']

    @pytest.mark.parametrize('logged, method, redirect, status, response_path, reason, error', [
        (False, 'get', True, 200, '/login', 'Login required.', ''),
        (True, 'get', True, 500, '/error', 'Found', 'Oops...Something went wrong.'),
        (False, 'post', False, 500, '/some_bad_room_name', '', 'Oops...Something went wrong.'),
        (True, 'post', False, 500, '/some_bad_room_name', '', 'Oops...Something went wrong.')
    ])
    async def test_bad_room(self, client, logged_client, logged, method, redirect, status, response_path, reason,
                            error):

        cl = client
        if logged:
            cl = logged_client

        response = await cl.__getattribute__(method)('/room/some_bad_room_name', data={'some_key': 'some_value'})
        assert response.status == status

        if error:
            assert error in await response.text()

        if redirect:
            assert len(response.history) == 1
            assert response.history[0].status == 302
            assert response.history[0].reason == reason
        else:
            assert len(response.history) == 0
        assert path(response.url) == response_path

    @pytest.mark.parametrize('logged, method, redirect, status, response_path, reason, error', [
        (False, 'get', False, 500, '/some_bad_url', '', 'Oops...Something went wrong.'),
        (True, 'get', False, 500, '/some_bad_url', '', 'Oops...Something went wrong.'),
        (False, 'post', False, 500, '/some_bad_url', '', 'Oops...Something went wrong.'),
        (True, 'post', False, 500, '/some_bad_url', '', 'Oops...Something went wrong.')
    ])
    async def test_bad_url(self, client, logged_client, logged, method, redirect, status, response_path, reason, error):

        cl = client
        if logged:
            cl = logged_client

        response = await cl.__getattribute__(method)('/some_bad_url', data={'some_key': 'some_value'})
        assert response.status == status

        if error:
            assert error in await response.text()

        if redirect:
            assert len(response.history) == 1
            assert response.history[0].status == 302
            assert response.history[0].reason == reason
        else:
            assert len(response.history) == 0

        assert path(response.url) == response_path


class TestDb:

    async def test_db_initialization(self, get_test_db):
        db = get_test_db.chat_db
        assert db.name == 'chat_test'
        assert await db.users.find().to_list(length=100) == []
        assert await db.rooms.find().to_list(length=100) == []

    async def test_db(self, get_filled_db):
        db = get_filled_db
        rooms = await db.rooms.find({}).to_list(length=100)

        assert len(rooms) == 4

        assert rooms[0]['room_name'] == 'Test_room'
        assert rooms[0]['slug_name'] == 'test_room'

        assert rooms[1]['room_name'] == 'Test_room.2'
        assert rooms[1]['slug_name'] == 'test_room_2'

        assert rooms[2]['room_name'] == 'Test room 3'
        assert rooms[2]['slug_name'] == 'test_room_3'

        assert rooms[3]['room_name'] == 'Test!room?4'
        assert rooms[3]['slug_name'] == 'test_room_4'

        users = await db.users.find({}).to_list(length=100)

        assert len(users) == 2

        assert users[0]['login'] == 'Test_User'
        assert users[0]['password'] != '123123'

        assert users[1]['login'] == 'Test User'
        assert users[1]['password'] != '123123'

    @pytest.mark.parametrize('room_name', [
        'Test room',
        'Test.room',
        'Test!room',
        'Test?room',
        'test_room',
        'test room'
    ])
    async def test_db_room_exceptions(self, get_filled_db, room_name):
        db = get_filled_db

        with pytest.raises(RoomExists):
            await db.create_room(room_name)

    @pytest.mark.parametrize('user_data', [
        {'login': 'Test User', 'password': '123123'},
    ])
    async def test_db_user_exceptions(self, get_filled_db, user_data):
        db = get_filled_db

        with pytest.raises(UserExists):
            await db.create_user(user_data)
